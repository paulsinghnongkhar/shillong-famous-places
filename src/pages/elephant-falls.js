import { graphql } from 'gatsby'
import React from 'react'
import Layout from '../component/Layout';
import ScrollToTop from "react-scroll-to-top";

function ElephantFalls({data}) {

const node1 = data.allNodeFamousPlaces.nodes || [];
const node2 = data.allParagraphPlaceParagraph.nodes[0];
const node6 = data.allParagraphPlaceParagraph.nodes[1];
const node3 = data.allBlockContentMap.nodes || [];
const node4 = data.allParagraphContactPlacesParagraph.nodes || [];
const node5 = data.allParagraphHeaderPlaceParagraph.nodes [0];
const node7 = data.allParagraphHeaderPlaceParagraph.nodes [1];
const node8 = data.allParagraphHeaderPlaceParagraph.nodes [2];
  return (
<Layout>
<title>Shillong Famous Places</title>
<div>
      { node1.length !== 0 ?
        node1.map((node, index) => (
            <div key={index}>
              <h1 className="py-5 mt-5 text-5xl text-center text-black border-2 border-black">{node.title}</h1>
                <img className="w-full" src={node.relationships.field_background_.publicUrl} />
                <center>
                <p className="font-sans text-3xl italic text-center text-black" dangerouslySetInnerHTML={{__html:node.body.processed}}></p>
                </center>
            </div>
        )) : <h1>NO DATA FOUND</h1>
      }
    </div>
    <div className="grid grid-cols-1 md:grid-cols-3 px-10 gap-x-5">
      { node5.length !== 0 ?
        (
          <div className="border-2 border-black text-center py-5 px-5">
            <center>
            <img src="/location.png" className="w-1/5" />
            </center>
            <h1 className="leading-7 font-bold text-xl">{node5.field_title_header_paragraph}
            <p className="font-light text-xl">{node5.field_header_places_description}</p>
            </h1>
          </div>
        ) : <h1>NO DATA FOUND</h1>
      }
      { node7.length !== 0 ?
        (
          <div className="border-2 border-black text-center py-5 px-5">
            <center>
            <img src="/sun.png" className="w-1/5" />
            </center>
            <h1 className="leading-7 font-bold text-xl">{node7.field_title_header_paragraph}
            <p className="font-light text-xl">{node7.field_header_places_description}</p>
            </h1>
          </div>
        ) : <h1>NO DATA FOUND</h1>
      }
      { node8.length !== 0 ?
        (
          <div className="border-2 border-black text-center py-5 px-5">
            <center>
            <img src="/airplane.png" className="w-1/5" />
            </center>
            <h1 className="leading-7 font-bold text-xl">{node8.field_title_header_paragraph}
            <p className="font-light text-xl">{node8.field_header_places_description}</p>
            </h1>
          </div>
        ) : <h1>NO DATA FOUND</h1>
      }
    </div>
    <div className="px-10 py-5">
      { node2.length !== 0 ?
        (
            <div className="grid grid-cols-1 md:grid-cols-2 gap-x-10">
                <p className="text-xl leading-10 text-justify text-black bg-gray-300 rounded-tl-full rounded-br-full" 
                dangerouslySetInnerHTML={{__html:node2.field_body.processed}} />
                <img className="w-full h-full" src={node2.relationships.field_upload_image.publicUrl} />
            </div>
        ) : <h1>NO DATA FOUND</h1>
      }
      { node6.length !== 0 ?
        (
            <div className="grid grid-cols-1 md:grid-cols-2 gap-x-10 mt-5 mb-5">
              <img className="w-full h-full" src={node6.relationships.field_upload_image.publicUrl} />
                <p className="text-xl leading-10 text-justify text-black bg-gray-300 rounded-tl-full rounded-br-full" 
                dangerouslySetInnerHTML={{__html:node6.field_body.processed}} />
            </div>
        ) : <h1>NO DATA FOUND</h1>
      }
    <hr className="bg-gray-800 h-0.5" />
    </div>
<div className="px-4 py-5 sm:px-6 lg:px-8">
<div className="flex flex-col justify-between sm:flex-row">
    <div className="grid grid-cols-1 md:grid-cols-2">
    {
        node4.length !== 0 ?
        node4.map((node, index) => (
        <div key={index} className="self-center">
          <h1 className="text-xl text-black">{node.field_title_places_location}
          <p className="font-light">{node.field_description_places}</p>
          </h1>
        </div>
        )) : <h1>NO DATA FOUND</h1>
      }
    </div>
      {
        node3.length !== 0 ?
        node3.map((node, index) => (
        <div key={index}>
          <h1 className="text-xl text-black">{node.field_location.title}</h1>
          <iframe src={node.field_location.uri} width="100%" height="300" />
        </div>
        )) : <h1>NO DATA FOUND</h1>
      }
    </div>
</div>
    <ScrollToTop smooth />
</Layout>
  )
}

export const query = graphql`
query {
    allNodeFamousPlaces(filter: {id: {eq: "cb42702b-cfdb-5172-9b95-84d46a2a195f"}}) {
      nodes {
        relationships {
          field_background_ {
            publicUrl
          }
        }
        title
        body {
          processed
        }
      }
    }
    allParagraphPlaceParagraph(
      filter: {id: {in: ["a6eef242-7cc5-51c4-b693-8138c8e2154d", "3afce971-39a9-5294-ace0-23d82e34b360"]}}
    ) {
      nodes {
        field_body {
          processed
        }
        relationships {
          field_upload_image {
            publicUrl
          }
        }
      }
    }
    allBlockContentMap(filter: {id: {eq: "ed2dc9d4-29ee-5187-bb87-7387ba5602fd"}}) {
      nodes {
        field_location {
          title
          uri
        }
      }
    }
    allParagraphContactPlacesParagraph(filter: {id: {in: 
      ["c01bf892-5ed5-590a-9b1a-bd8e2c3fe51c","406efefd-4aac-5348-b10e-d70a54f04d44","e1f70991-e0d5-599b-b1e0-113bdac0101f","fa626558-e772-5088-a297-bceb682f566c"]}}) {
      nodes {
        field_title_places_location
        field_description_places
      }
    }
    allParagraphHeaderPlaceParagraph(
      filter: {id: {in: ["fd9a959c-11a5-5894-b2e6-e56c7ea05eeb", "73e5039d-8cef-5d42-b1de-c68391571217", "9fa294ed-2c60-5cf1-bcfb-08b230050c49"]}}
    ) {
      nodes {
        field_title_header_paragraph
        field_header_places_description
      }
    }
  }
`

export default ElephantFalls
