import { graphql } from 'gatsby'
import React from 'react'
import Layout from '../component/Layout';
import ScrollToTop from "react-scroll-to-top";

function shillongheritagewalk({data}) {

const node1 = data.allNodeFamousPlaces.nodes || [];
const node2 = data.allParagraphPlaceParagraph.nodes[0];
const node6 = data.allParagraphPlaceParagraph.nodes[1];
const node3 = data.allBlockContentMap.nodes || [];
const node4 = data.allParagraphContactPlacesParagraph.nodes || [];
const node5 = data.allParagraphHeaderPlaceParagraph.nodes [0];
const node7 = data.allParagraphHeaderPlaceParagraph.nodes [1];
const node8 = data.allParagraphHeaderPlaceParagraph.nodes [2];

return (
  <Layout>
  <title>Shillong Famous Places</title>
  <div>
        { node1.length !== 0 ?
          node1.map((node, index) => (
              <div key={index}>
              <h1 className="py-5 mt-5 text-5xl text-center text-black border-2 border-black">{node.title}</h1>
                  <img className="w-full" src={node.relationships.field_background_.publicUrl} />
                  <center>
                  <p className="font-sans text-3xl italic text-center text-black" dangerouslySetInnerHTML={{__html:node.body.processed}}></p>
                  </center>
              </div>
          )) : <h1>NO DATA FOUND</h1>
        }
      </div>
      <div className="grid grid-cols-1 md:grid-cols-2 px-10 gap-x-5">
        { node5.length !== 0 ?
          (
            <div className="border-2 border-black text-center py-5 px-5">
              <center>
              <img src="/location.png" className="w-1/5" />
              </center>
              <h1 className="leading-7 font-bold text-xl">{node5.field_title_header_paragraph}
              <p className="font-light text-xl">{node5.field_header_places_description}</p>
              </h1>
            </div>
          ) : <h1>NO DATA FOUND</h1>
        }
        { node7.length !== 0 ?
          (
            <div className="border-2 border-black text-center py-5 px-5">
              <center>
              <img src="/sun.png" className="w-1/5" />
              </center>
              <h1 className="leading-7 font-bold text-xl">{node7.field_title_header_paragraph}
              <p className="font-light text-xl">{node7.field_header_places_description}</p>
              </h1>
            </div>
          ) : <h1>NO DATA FOUND</h1>
        }
      </div>
      <div className="px-10 py-5">
        { node2.length !== 0 ?
          (
              <div className="grid grid-cols-1 md:grid-cols-2 gap-x-10">
                  <p className="text-xl leading-10 text-justify text-black bg-gray-300 rounded-tl-full rounded-br-full" 
                  dangerouslySetInnerHTML={{__html:node2.field_body.processed}} />
                  <img className="w-full h-full" src={node2.relationships.field_upload_image.publicUrl} />
              </div>
          ) : <h1>NO DATA FOUND</h1>
        }
        { node6.length !== 0 ?
          (
              <div className="grid grid-cols-1 md:grid-cols-2 gap-x-10 mt-5 mb-5">
                <img className="w-full h-full" src={node6.relationships.field_upload_image.publicUrl} />
                  <p className="text-xl leading-10 text-justify text-black bg-gray-300 rounded-tl-full rounded-br-full" 
                  dangerouslySetInnerHTML={{__html:node6.field_body.processed}} />
              </div>
          ) : <h1>NO DATA FOUND</h1>
        }
      <hr className="bg-gray-800 h-0.5" />
      </div>
  <div className="px-4 py-5 sm:px-6 lg:px-8">
  <div className="flex flex-col justify-between sm:flex-row">
      <div className="grid grid-cols-1 md:grid-cols-2">
      {
          node4.length !== 0 ?
          node4.map((node, index) => (
          <div key={index} className="self-center">
            <h1 className="text-xl text-black">{node.field_title_places_location}
            <p className="font-light">{node.field_description_places}</p>
            </h1>
          </div>
          )) : <h1>NO DATA FOUND</h1>
        }
      </div>
        {
          node3.length !== 0 ?
          node3.map((node, index) => (
          <div key={index}>
            <h1 className="text-xl text-black">{node.field_location.title}</h1>
            <iframe src={node.field_location.uri} width="100%" height="300" />
          </div>
          )) : <h1>NO DATA FOUND</h1>
        }
      </div>
  </div>
      <ScrollToTop smooth />
  </Layout>
    )
  }

export const query = graphql
`
query {
    allNodeFamousPlaces(filter: {id: {eq: "f50557c1-902b-56ca-ad59-983cfcab7517"}}) {
      nodes {
        relationships {
          field_background_ {
            publicUrl
          }
        }
        title
        body {
          processed
        }
      }
    }
    allParagraphPlaceParagraph(
      filter: {id: {in: ["9e7dd59b-ab68-5554-bcac-1351750a285f", "68120cb8-30f4-58f1-a49e-de63c37cc064"]}}
    ) {
      nodes {
        field_body {
          processed
        }
        relationships {
          field_upload_image {
            publicUrl
          }
        }
        id
      }
    }
    allBlockContentMap(filter: {id: {eq: "5af4c191-6c66-52dc-94a8-9f91b7288160"}}) {
      nodes {
        field_location {
          title
          uri
        }
      }
    }
    allParagraphContactPlacesParagraph(
      filter: {id: {in: ["dc4b04ba-60d2-585a-bb6f-3d25afc3f9d8", "8abbf3ea-96a7-5666-9908-085ad40bbee7", "0cb5fd4c-650f-5f1e-9c6a-70cffc769d9a", "ad12bb85-9f00-5551-acae-e81055a09a0b"]}}
    ) {
      nodes {
        field_title_places_location
        field_description_places
        id
      }
    }
    allParagraphHeaderPlaceParagraph(
      filter: {id: {in: ["7cdf6ebd-8079-53e8-87a2-67ff4bbb7e63", "4c3da8ab-d2e2-5044-8146-fc71b0841693"]}}
    ) {
      nodes {
        field_title_header_paragraph
        field_header_places_description
        id
      }
    }
  }
`

export default shillongheritagewalk
