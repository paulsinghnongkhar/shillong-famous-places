import React from 'react'
import Layout from '../component/Layout'
import { Link, graphql } from 'gatsby'
import ScrollToTop from "react-scroll-to-top";

function discover({data}) {
  const node1 = data.allNodeMenu.nodes || [];
  const node2 = data.allNodeDiscover.nodes || [];

  return (
    <Layout>
      <title>Shillong Famous Places</title>
      <div>
        {node1.length !== 0 ?
          node1.map((nod, index) => (
            <div key={index}>
              <h1 className="py-5 mt-5 text-5xl text-center text-black border-2 border-black">{nod.title}</h1>
              <img className="w-full h-auto" src={nod.relationships.field_image_menu.publicUrl} alt={nod.title} />
            </div>
          )) : <h1>NO DATA FOUND</h1>
        }
      </div>
      <div className="container grid grid-cols-1 gap-4 px-4 py-4 mx-auto bg-gray-300 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-3">
        {node2.length !== 0 ?
          node2.map((noda, index) => (
            <div key={index} className="border-2 border-black">
              <Link to={noda.path.alias} className="no-underline hover:opacity-50">
                <p className="text-xl leading-10 text-center text-black">
                  <img className="w-full h-auto" src={noda.relationships.field_discover_header_image.publicUrl} alt={noda.title} />
                  {noda.title}
                </p>
              </Link>
            </div>
          )) : <h1>NO DATA FOUND</h1>
        }
      </div>
      <ScrollToTop smooth />
    </Layout>
  )
}

export const query = graphql
`
query {
    allNodeMenu(filter: {id: {eq: "6f853ecd-9d97-5973-8f83-db2c6a8d597b"}}) {
      nodes {
        title
        relationships {
          field_image_menu {
            publicUrl
          }
        }
      }
    }
    allNodeDiscover {
        nodes {
          title
          path {
            alias
          }
          relationships {
            field_discover_header_image {
              publicUrl
            }
          }
        }
      }
  }
`

export default discover
