import React from 'react'
import Layout from '../component/Layout'
import { graphql } from 'gatsby'
import ScrollToTop from "react-scroll-to-top";

function golflinks({data}) {

    const node1 = data.allNodeFamousPlaces.nodes || [];
    const node2 = data.allParagraphPlaceParagraph.nodes[0];
    const node6 = data.allParagraphPlaceParagraph.nodes[1];
    const node3 = data.allBlockContentMap.nodes || [];
    const node4 = data.allParagraphContactPlacesParagraph.nodes || [];
    const node5 = data.allParagraphHeaderPlaceParagraph.nodes [0];
    const node7 = data.allParagraphHeaderPlaceParagraph.nodes [1];
    const node8 = data.allParagraphHeaderPlaceParagraph.nodes [2];

    return (
      <Layout>
      <title>Shillong Famous Places</title>
      <div>
            { node1.length !== 0 ?
              node1.map((node, index) => (
                  <div key={index}>
              <h1 className="py-5 mt-5 text-5xl text-center text-black border-2 border-black">{node.title}</h1>
                      <img className="w-full" src={node.relationships.field_background_.publicUrl} />
                      <center>
                      <p className="font-sans text-3xl italic text-center text-black" dangerouslySetInnerHTML={{__html:node.body.processed}}></p>
                      </center>
                  </div>
              )) : <h1>NO DATA FOUND</h1>
            }
          </div>
          <div className="grid grid-cols-1 md:grid-cols-3 px-10 gap-x-5">
            { node5.length !== 0 ?
              (
                <div className="border-2 border-black text-center py-5 px-5">
                  <center>
                  <img src="/location.png" className="w-1/5" />
                  </center>
                  <h1 className="leading-7 font-bold text-xl">{node5.field_title_header_paragraph}
                  <p className="font-light text-xl">{node5.field_header_places_description}</p>
                  </h1>
                </div>
              ) : <h1>NO DATA FOUND</h1>
            }
            { node7.length !== 0 ?
              (
                <div className="border-2 border-black text-center py-5 px-5">
                  <center>
                  <img src="/sun.png" className="w-1/5" />
                  </center>
                  <h1 className="leading-7 font-bold text-xl">{node7.field_title_header_paragraph}
                  <p className="font-light text-xl">{node7.field_header_places_description}</p>
                  </h1>
                </div>
              ) : <h1>NO DATA FOUND</h1>
            }
            { node8.length !== 0 ?
              (
                <div className="border-2 border-black text-center py-5 px-5">
                  <center>
                  <img src="/airplane.png" className="w-1/5" />
                  </center>
                  <h1 className="leading-7 font-bold text-xl">{node8.field_title_header_paragraph}
                  <p className="font-light text-xl">{node8.field_header_places_description}</p>
                  </h1>
                </div>
              ) : <h1>NO DATA FOUND</h1>
            }
          </div>
          <div className="px-10 py-5">
            { node2.length !== 0 ?
              (
                  <div className="grid grid-cols-1 md:grid-cols-2 gap-x-10">
                      <p className="text-xl leading-10 text-justify text-black bg-gray-300 rounded-tl-full rounded-br-full" 
                      dangerouslySetInnerHTML={{__html:node2.field_body.processed}} />
                      <img className="w-full h-full" src={node2.relationships.field_upload_image.publicUrl} />
                  </div>
              ) : <h1>NO DATA FOUND</h1>
            }
            { node6.length !== 0 ?
              (
                  <div className="grid grid-cols-1 md:grid-cols-2 gap-x-10 mt-5 mb-5">
                    <img className="w-full h-full" src={node6.relationships.field_upload_image.publicUrl} />
                      <p className="text-xl leading-10 text-justify text-black bg-gray-300 rounded-tl-full rounded-br-full" 
                      dangerouslySetInnerHTML={{__html:node6.field_body.processed}} />
                  </div>
              ) : <h1>NO DATA FOUND</h1>
            }
          <hr className="bg-gray-800 h-0.5" />
          </div>
      <div className="px-4 py-5 sm:px-6 lg:px-8">
      <div className="flex flex-col justify-between sm:flex-row">
          <div className="grid grid-cols-1 md:grid-cols-2">
          {
              node4.length !== 0 ?
              node4.map((node, index) => (
              <div key={index} className="self-center">
                <h1 className="text-xl text-black">{node.field_title_places_location}
                <p className="font-light">{node.field_description_places}</p>
                </h1>
              </div>
              )) : <h1>NO DATA FOUND</h1>
            }
          </div>
            {
              node3.length !== 0 ?
              node3.map((node, index) => (
              <div key={index}>
                <h1 className="text-xl text-black">{node.field_location.title}</h1>
                <iframe src={node.field_location.uri} width="100%" height="300" />
              </div>
              )) : <h1>NO DATA FOUND</h1>
            }
          </div>
      </div>
          <ScrollToTop smooth />
      </Layout>
        )
      }

export const query = graphql`
query {
    allNodeFamousPlaces(filter: {id: {eq: "bba54e26-e714-5a73-a644-3a9ee1846e96"}}) {
      nodes {
        relationships {
          field_background_ {
            publicUrl
          }
        }
        title
        body {
          processed
        }
      }
    }
    allParagraphPlaceParagraph(
      filter: {id: {in: ["379f7cd7-8b16-5b1a-a401-e09e92cee634", "c3179fb2-905c-5ef1-a340-12e8c892b635"]}}
    ) {
      nodes {
        field_body {
          processed
        }
        relationships {
          field_upload_image {
            publicUrl
          }
        }
        id
      }
    }
    allBlockContentMap(filter: {id: {eq: "a20c6a8c-d983-5ef3-ac78-af470e03ecc0"}}) {
      nodes {
        field_location {
          title
          uri
        }
      }
    }
    allParagraphContactPlacesParagraph(
      filter: {id: {in: ["6fbe7c96-59db-5cad-9b44-a1bea4cf986e", "ff85fc1b-f778-5dbc-a6b8-ed3f6e69b93e", "41596c5d-84cb-5673-807f-90301fad8455", "4e62b442-90ce-5b34-87bc-d25d94c64c0a"]}}
    ) {
      nodes {
        field_title_places_location
        field_description_places
        id
      }
    }
    allParagraphHeaderPlaceParagraph(
      filter: {id: {in: ["7072e21b-ff1d-517e-ba77-b434355b6369", "ac939842-00d0-59af-8d00-7eae19bf77d4", "133e068b-7735-5bd7-bd0f-2622578dde87"]}}
    ) {
      nodes {
        field_title_header_paragraph
        field_header_places_description
      }
    }
  }
`

export default golflinks
