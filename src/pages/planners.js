import React from 'react'
import Layout from '../component/Layout'
import { Link, graphql } from 'gatsby'
import ScrollToTop from "react-scroll-to-top";

function Planners({ data }) {
  const node1 = data.allNodeMenu.nodes || [];
  const node2 = data.allNodePlanner.nodes || [];

  return (
    <Layout>
      <title>Shillong Famous Places</title>
      <div>
        {node1.length !== 0 ? (
          node1.map((nod, index) => (
            <div key={index}>
              <h1 className="py-5 mt-5 text-5xl text-center text-black border-2 border-black">{nod.title}</h1>
              <img className="w-full h-auto" src={nod.relationships.field_image_menu.publicUrl} alt={nod.title} />
            </div>
          ))
        ) : (
          <h1>NO DATA FOUND</h1>
        )}
      </div>
      <div className="grid grid-cols-1 gap-2 px-5 py-5 bg-gray-300 md:grid-cols-3 lg:grid-cols-3 xl:grid-cols-3">
        {node2.length !== 0 ? (
          node2.map((noda, index) => (
            <div key={index} className="border-2 border-black">
              <Link to={noda.path.alias} className="no-underline hover:opacity-50">
                <p className="text-xl leading-10 text-center text-black">
                <img className="w-full h-auto" src={noda.relationships.field_header_image_planner.publicUrl} alt={noda.title} />
                  {noda.title}
                </p>
              </Link>
            </div>
          ))
        ) : (
          <h1>NO DATA FOUND</h1>
        )}
      </div>
      <ScrollToTop smooth />
    </Layout>
  )
}

export const query = graphql
`
query {
    allNodeMenu(filter: {id: {eq: "b03deea3-ad43-5040-886a-f4d8a7321944"}}) {
        nodes {
          title
          relationships {
            field_image_menu {
              publicUrl
            }
          }
        }
      }
      allNodePlanner(sort: {title: ASC}) {
        nodes {
          title
          path {
            alias
          }
          relationships {
            field_header_image_planner {
              publicUrl
            }
          }
        }
      }
  }
`

export default Planners
