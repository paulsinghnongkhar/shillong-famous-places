import React from 'react'
import Layout from '../component/Layout'
import { graphql } from 'gatsby'
import ScrollToTop from "react-scroll-to-top";

function phannonglaitladyhydaripark({data}) {

    const node1 = data.allNodeFamousPlaces.nodes || [];
    const node2 = data.allParagraphPlaceParagraph.nodes[0];
    const node6 = data.allParagraphPlaceParagraph.nodes[1];
    const node3 = data.allBlockContentMap.nodes || [];
    const node4 = data.allParagraphContactPlacesParagraph.nodes || [];
    const node5 = data.allParagraphHeaderPlaceParagraph.nodes [0];
    const node7 = data.allParagraphHeaderPlaceParagraph.nodes [1];
    const node8 = data.allParagraphHeaderPlaceParagraph.nodes [2];

    return (
      <Layout>
      <title>Shillong Famous Places</title>
      <div>
            { node1.length !== 0 ?
              node1.map((node, index) => (
                  <div key={index}>
              <h1 className="py-5 mt-5 text-5xl text-center text-black border-2 border-black">{node.title}</h1>
                      <img className="w-full" src={node.relationships.field_background_.publicUrl} />
                      <center>
                      <p className="font-sans text-3xl italic text-center text-black" dangerouslySetInnerHTML={{__html:node.body.processed}}></p>
                      </center>
                  </div>
              )) : <h1>NO DATA FOUND</h1>
            }
          </div>
          <div className="grid grid-cols-1 md:grid-cols-3 px-10 gap-x-5">
            { node5.length !== 0 ?
              (
                <div className="border-2 border-black text-center py-5 px-5">
                  <center>
                  <img src="/location.png" className="w-1/5" />
                  </center>
                  <h1 className="leading-7 font-bold text-xl">{node5.field_title_header_paragraph}
                  <p className="font-light text-xl">{node5.field_header_places_description}</p>
                  </h1>
                </div>
              ) : <h1>NO DATA FOUND</h1>
            }
            { node7.length !== 0 ?
              (
                <div className="border-2 border-black text-center py-5 px-5">
                  <center>
                  <img src="/sun.png" className="w-1/5" />
                  </center>
                  <h1 className="leading-7 font-bold text-xl">{node7.field_title_header_paragraph}
                  <p className="font-light text-xl">{node7.field_header_places_description}</p>
                  </h1>
                </div>
              ) : <h1>NO DATA FOUND</h1>
            }
            { node8.length !== 0 ?
              (
                <div className="border-2 border-black text-center py-5 px-5">
                  <center>
                  <img src="/airplane.png" className="w-1/5" />
                  </center>
                  <h1 className="leading-7 font-bold text-xl">{node8.field_title_header_paragraph}
                  <p className="font-light text-xl">{node8.field_header_places_description}</p>
                  </h1>
                </div>
              ) : <h1>NO DATA FOUND</h1>
            }
          </div>
          <div className="px-10 py-5">
            { node2.length !== 0 ?
              (
                  <div className="grid grid-cols-1 md:grid-cols-2 gap-x-10">
                      <p className="text-xl leading-10 text-justify text-black bg-gray-300 rounded-tl-full rounded-br-full" 
                      dangerouslySetInnerHTML={{__html:node2.field_body.processed}} />
                      <img className="w-full h-full" src={node2.relationships.field_upload_image.publicUrl} />
                  </div>
              ) : <h1>NO DATA FOUND</h1>
            }
            { node6.length !== 0 ?
              (
                  <div className="grid grid-cols-1 md:grid-cols-2 gap-x-10 mt-5 mb-5">
                    <img className="w-full h-full" src={node6.relationships.field_upload_image.publicUrl} />
                      <p className="text-xl leading-10 text-justify text-black bg-gray-300 rounded-tl-full rounded-br-full" 
                      dangerouslySetInnerHTML={{__html:node6.field_body.processed}} />
                  </div>
              ) : <h1>NO DATA FOUND</h1>
            }
          <hr className="bg-gray-800 h-0.5" />
          </div>
      <div className="px-4 py-5 sm:px-6 lg:px-8">
      <div className="flex flex-col justify-between sm:flex-row">
          <div className="grid grid-cols-1 md:grid-cols-2">
          {
              node4.length !== 0 ?
              node4.map((node, index) => (
              <div key={index} className="self-center">
                <h1 className="text-xl text-black">{node.field_title_places_location}
                <p className="font-light">{node.field_description_places}</p>
                </h1>
              </div>
              )) : <h1>NO DATA FOUND</h1>
            }
          </div>
            {
              node3.length !== 0 ?
              node3.map((node, index) => (
              <div key={index}>
                <h1 className="text-xl text-black">{node.field_location.title}</h1>
                <iframe src={node.field_location.uri} width="100%" height="300" />
              </div>
              )) : <h1>NO DATA FOUND</h1>
            }
          </div>
      </div>
          <ScrollToTop smooth />
      </Layout>
        )
      }

export const query = graphql`
query {
    allNodeFamousPlaces(filter: {id: {eq: "a771ec4e-552d-5bce-9643-05c433f96c64"}}) {
      nodes {
        relationships {
          field_background_ {
            publicUrl
          }
        }
        title
        body {
          processed
        }
      }
    }
    allParagraphPlaceParagraph(
      filter: {id: {in: ["b8a4735d-3e68-55da-abdf-0be6e0e770f1", "f577286c-bc97-5804-8681-ff0522d3e809"]}}
    ) {
      nodes {
        field_body {
          processed
        }
        relationships {
          field_upload_image {
            publicUrl
          }
        }
        id
      }
    }
    allBlockContentMap(filter: {id: {eq: "126ed3f2-399c-50f7-9b1c-8042646175be"}}) {
      nodes {
        field_location {
          title
          uri
        }
      }
    }
    allParagraphContactPlacesParagraph(
      filter: {id: {in: ["1a6cdfec-9dbb-5365-bf09-a78c866970dd", "17e4af14-8f89-5dfd-9220-1784f366aaaf", "7a381917-820e-594d-958c-4f43b038573b", "b93c5a97-9cc2-599a-9716-215cf20f14d9"]}}
    ) {
      nodes {
        field_title_places_location
        field_description_places
        id
      }
    }
    allParagraphHeaderPlaceParagraph(
      filter: {id: {in: ["1765f799-1a27-5f45-9056-b5c1b7670c33", "789d5907-96b8-590a-b326-3ee6e79ad55d", "54d54ece-44ef-55a5-97e1-8ace17ba0496"]}}
    ) {
      nodes {
        field_title_header_paragraph
        field_header_places_description
      }
    }
  }
`

export default phannonglaitladyhydaripark
