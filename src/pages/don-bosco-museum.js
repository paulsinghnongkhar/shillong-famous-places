import React from 'react'
import Layout from '../component/Layout'
import { graphql } from 'gatsby'
import ScrollToTop from "react-scroll-to-top";

function donboscomuseum({data}) {

    const node1 = data.allNodeFamousPlaces.nodes || [];
    const node2 = data.allParagraphPlaceParagraph.nodes[0];
    const node6 = data.allParagraphPlaceParagraph.nodes[1];
    const node3 = data.allBlockContentMap.nodes || [];
    const node4 = data.allParagraphContactPlacesParagraph.nodes || [];
    const node5 = data.allParagraphHeaderPlaceParagraph.nodes [0];
    const node7 = data.allParagraphHeaderPlaceParagraph.nodes [1];
    const node8 = data.allParagraphHeaderPlaceParagraph.nodes [2];

    return (
      <Layout>
      <title>Shillong Famous Places</title>
      <div>
            { node1.length !== 0 ?
              node1.map((node, index) => (
                  <div key={index}>
              <h1 className="py-5 mt-5 text-5xl text-center text-black border-2 border-black">{node.title}</h1>
                      <img className="w-full" src={node.relationships.field_background_.publicUrl} />
                      <center>
                      <p className="font-sans text-3xl italic text-center text-black" dangerouslySetInnerHTML={{__html:node.body.processed}}></p>
                      </center>
                  </div>
              )) : <h1>NO DATA FOUND</h1>
            }
          </div>
          <div className="grid grid-cols-1 md:grid-cols-3 px-10 gap-x-5">
            { node5.length !== 0 ?
              (
                <div className="border-2 border-black text-center py-5 px-5">
                  <center>
                  <img src="/location.png" className="w-1/5" />
                  </center>
                  <h1 className="leading-7 font-bold text-xl">{node5.field_title_header_paragraph}
                  <p className="font-light text-xl">{node5.field_header_places_description}</p>
                  </h1>
                </div>
              ) : <h1>NO DATA FOUND</h1>
            }
            { node7.length !== 0 ?
              (
                <div className="border-2 border-black text-center py-5 px-5">
                  <center>
                  <img src="/sun.png" className="w-1/5" />
                  </center>
                  <h1 className="leading-7 font-bold text-xl">{node7.field_title_header_paragraph}
                  <p className="font-light text-xl">{node7.field_header_places_description}</p>
                  </h1>
                </div>
              ) : <h1>NO DATA FOUND</h1>
            }
            { node8.length !== 0 ?
              (
                <div className="border-2 border-black text-center py-5 px-5">
                  <center>
                  <img src="/airplane.png" className="w-1/5" />
                  </center>
                  <h1 className="leading-7 font-bold text-xl">{node8.field_title_header_paragraph}
                  <p className="font-light text-xl">{node8.field_header_places_description}</p>
                  </h1>
                </div>
              ) : <h1>NO DATA FOUND</h1>
            }
          </div>
          <div className="px-10 py-5">
            { node2.length !== 0 ?
              (
                  <div className="grid grid-cols-1 md:grid-cols-2 gap-x-10">
                      <p className="text-xl leading-10 text-justify text-black bg-gray-300 rounded-tl-full rounded-br-full" 
                      dangerouslySetInnerHTML={{__html:node2.field_body.processed}} />
                      <img className="w-full h-full" src={node2.relationships.field_upload_image.publicUrl} />
                  </div>
              ) : <h1>NO DATA FOUND</h1>
            }
            { node6.length !== 0 ?
              (
                  <div className="grid grid-cols-1 md:grid-cols-2 gap-x-10 mt-5 mb-5">
                    <img className="w-full h-full" src={node6.relationships.field_upload_image.publicUrl} />
                      <p className="text-xl leading-10 text-justify text-black bg-gray-300 rounded-tl-full rounded-br-full" 
                      dangerouslySetInnerHTML={{__html:node6.field_body.processed}} />
                  </div>
              ) : <h1>NO DATA FOUND</h1>
            }
          <hr className="bg-gray-800 h-0.5" />
          </div>
      <div className="px-4 py-5 sm:px-6 lg:px-8">
      <div className="flex flex-col justify-between sm:flex-row">
          <div className="grid grid-cols-1 md:grid-cols-2">
          {
              node4.length !== 0 ?
              node4.map((node, index) => (
              <div key={index} className="self-center">
                <h1 className="text-xl text-black">{node.field_title_places_location}
                <p className="font-light">{node.field_description_places}</p>
                </h1>
              </div>
              )) : <h1>NO DATA FOUND</h1>
            }
          </div>
            {
              node3.length !== 0 ?
              node3.map((node, index) => (
              <div key={index}>
                <h1 className="text-xl text-black">{node.field_location.title}</h1>
                <iframe src={node.field_location.uri} width="100%" height="300" />
              </div>
              )) : <h1>NO DATA FOUND</h1>
            }
          </div>
      </div>
          <ScrollToTop smooth />
      </Layout>
        )
      }

export const query = graphql`
query {
    allNodeFamousPlaces(filter: {id: {eq: "e9494e89-5ad1-503a-bec7-b08756fc8c92"}}) {
      nodes {
        relationships {
          field_background_ {
            publicUrl
          }
        }
        title
        body {
          processed
        }
      }
    }
    allParagraphPlaceParagraph(
      filter: {id: {in: ["0c2c1dc9-831e-5593-a6e0-4e147997f302", "5e0eb30a-5c57-5f61-bb57-6a6272a24ac5"]}}
    ) {
      nodes {
        field_body {
          processed
        }
        relationships {
          field_upload_image {
            publicUrl
          }
        }
        id
      }
    }
    allBlockContentMap(filter: {id: {eq: "132f11c1-750d-5555-a8c6-1fabe07119e3"}}) {
      nodes {
        field_location {
          title
          uri
        }
      }
    }
    allParagraphContactPlacesParagraph(filter: {id: {in: ["d2d40736-d162-5071-8eb4-e5941ffc6ec7","92a4c165-af89-5b3e-ba2d-763e94b110e2","71bb836e-f8db-5584-808a-f7e22773379c","1b49ad44-7114-5a9c-94af-a09ecbff737f"]}}) {
      nodes {
        field_title_places_location
        field_description_places
        id
      }
    }
    allParagraphHeaderPlaceParagraph(
      filter: {id: {in: ["be4eab1c-9c1a-5767-b68c-c5a6406587c2", "c6013d67-e28b-51c6-9ff9-ff8852901da8", "ce4ae227-da5f-5a4c-b007-7f0e68272835"]}}
    ) {
      nodes {
        field_title_header_paragraph
        field_header_places_description
      }
    }
  }
`

export default donboscomuseum
