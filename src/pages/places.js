import React from 'react';
import Layout from '../component/Layout';
import { Link, graphql } from 'gatsby';
import ScrollToTop from "react-scroll-to-top";

const MyComponent = ({ data }) => {

  const node2 = data.allNodePlaces.nodes || [];
  const node3 = data.allNodeFamousPlaces.nodes || [];

  return (
    <Layout>
      <title>Shillong Famous Places</title>
      <div>
        {node2.length !== 0 ?
          node2.map((nod, index) => (
            <div key={index}>
              <h1 className="py-5 mt-5 text-5xl text-center text-black border-2 border-black">{nod.title}</h1>
              <img className="w-full h-auto" src={nod.relationships.field_background_image.publicUrl} alt={nod.title} />
              <div className="px-10 py-10 mt-10">
                <center className="py-5 bg-gray-300 rounded-tl-full rounded-br-full">
                  <p className="text-xl text-justify text-black" dangerouslySetInnerHTML={{ __html: nod.body.processed }} />
                </center>
              </div>
            </div>
          )) : <h1>NO DATA FOUND</h1>
        }
      </div>
      <div className="grid grid-cols-1 gap-2 px-5 py-5 mt-10 bg-gray-300 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-3 xl:grid-cols-3">
        {node3.length !== 0 ?
          node3.map((noda, index) => (
            <div key={index} className="border-2 border-black">
              <Link to={noda.path.alias} className="no-underline hover:opacity-50">
                <p className="leading-10 text-center text-black">
                  <img className="w-full h-auto" src={noda.relationships.field_background_.publicUrl} alt={noda.title} />
                  {noda.title}
                </p>
              </Link>
            </div>
          )) : <h1>NO DATA FOUND</h1>
        }
      </div>
      <ScrollToTop smooth />
    </Layout>
  );
};

export const query = graphql`
query {
    allNodePlaces {
      nodes {
        title
        body {
          processed
        }
        relationships {
          field_background_image {
            publicUrl
          }
        }
      }
    }
    allNodeFamousPlaces {
      nodes {
        relationships {
          field_background_ {
            publicUrl
          }
        }
        path {
          alias
        }
        title
      }
    }
  }
`

export default MyComponent;
