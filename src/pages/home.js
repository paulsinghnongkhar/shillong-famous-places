import React from 'react';
import Layout from '../component/Layout';
import { Carousel } from 'react-carousel-minimal';
import { Link, graphql } from 'gatsby';
import ScrollToTop from 'react-scroll-to-top';

function Index({ data }) {
  const node = data.allParagraphSlider.nodes || [];
  const node2 = data.allNodeHome.nodes || [];
  const node3 = data.allNodeMenu.nodes || [];

  const slider = node.map((element) => ({
    image: element.relationships.field_slider_image.publicUrl,
  }));

  return (
    <Layout>
      <title>Shillong Famous Places</title>
      <div className="container mx-auto">
        <Carousel
          data={slider}
          time={5000}
          width="100%"
          height="500px"
          slideNumber={true}
          slideNumberStyle={{
            fontSize: '20px',
            fontWeight: 'bold',
          }}
          captionPosition="bottom"
          automatic={true}
          dots={true}
          pauseIconColor="white"
          pauseIconSize="40px"
          slideBackgroundColor="darkgrey"
          slideImageFit="cover"
          style={{
            textAlign: 'center',
            maxWidth: '100%',
            maxHeight: '50%',
          }}
        />
      </div>
      <div className="container mx-auto mt-5">
        {node2.length !== 0 ? (
          node2.map((node, index) => (
            <div key={index} className="px-4 py-4 sm:px-10 sm:py-10">
              <h1 className="text-3xl font-bold text-center text-black sm:text-4xl md:text-5xl lg:text-5xl">
                {node.field_header}
              </h1>
              <div className="px-4 py-4 border-2 border-black sm:px-10 sm:py-10">
                <img
                  className="w-full"
                  src={node.relationships.field_image_map.publicUrl}
                  alt={node.field_header}
                />
              </div>
              <center>
              <p className="mt-5 text-3xl font-bold text-black sm:text-4xl md:text-5xl lg:text-4xl" dangerouslySetInnerHTML={{ __html: node.body.processed }} />
              </center>
              
            </div>
          ))
        ) : (
          <h1>NO DATA FOUND</h1>
        )}
      </div>
      <div className="container grid grid-cols-1 gap-2 px-4 py-4 mx-auto bg-gray-300 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-3">
        {node3.length !== 0 ? (
          node3.map((noda, index) => (
            <div key={index} className="border-2 border-black">
              <Link to={noda.path.alias} className="no-underline hover:opacity-50">
                <p className="text-3xl leading-10 text-center text-black sm:text-4xl md:text-5xl lg:text-3xl">
                  <img className="w-full" src={noda.relationships.field_image_menu.publicUrl} alt={noda.title} />
                  {noda.title}
                </p>
              </Link>
            </div>
          ))
        ) : (
          <h1>NO DATA FOUND</h1>
        )}
      </div>
      <ScrollToTop smooth />
    </Layout>
  );
}

export const query = graphql`
  query {
    allParagraphSlider {
      nodes {
        relationships {
          field_slider_image {
            publicUrl
          }
        }
      }
    }
    allNodeHome {
      nodes {
        field_header
        relationships {
          field_image_map {
            publicUrl
          }
        }
        body {
          processed
        }
      }
    }
    allNodeMenu {
      nodes {
        title
        path {
          alias
        }
        relationships {
          field_image_menu {
            publicUrl
          }
        }
      }
    }
  }
`;

export default Index;
