import React from 'react'
import Layout from '../component/Layout'
import { graphql } from 'gatsby'
import ScrollToTop from "react-scroll-to-top";

function wardslake({data}) {

    const node1 = data.allNodeFamousPlaces.nodes || [];
    const node2 = data.allParagraphPlaceParagraph.nodes[0];
    const node6 = data.allParagraphPlaceParagraph.nodes[1];
    const node3 = data.allBlockContentMap.nodes || [];
    const node4 = data.allParagraphContactPlacesParagraph.nodes || [];
    const node5 = data.allParagraphHeaderPlaceParagraph.nodes [0];
    const node7 = data.allParagraphHeaderPlaceParagraph.nodes [1];
    const node8 = data.allParagraphHeaderPlaceParagraph.nodes [2];
    
    return (
      <Layout>
      <title>Shillong Famous Places</title>
      <div>
            { node1.length !== 0 ?
              node1.map((node, index) => (
                  <div key={index}>
              <h1 className="py-5 mt-5 text-5xl text-center text-black border-2 border-black">{node.title}</h1>
                      <img className="w-full" src={node.relationships.field_background_.publicUrl} />
                      <center>
                      <p className="font-sans text-3xl italic text-center text-black" dangerouslySetInnerHTML={{__html:node.body.processed}}></p>
                      </center>
                  </div>
              )) : <h1>NO DATA FOUND</h1>
            }
          </div>
          <div className="grid grid-cols-1 md:grid-cols-3 px-10 gap-x-5">
            { node5.length !== 0 ?
              (
                <div className="border-2 border-black text-center py-5 px-5">
                  <center>
                  <img src="/location.png" className="w-1/5" />
                  </center>
                  <h1 className="leading-7 font-bold text-xl">{node5.field_title_header_paragraph}
                  <p className="font-light text-xl">{node5.field_header_places_description}</p>
                  </h1>
                </div>
              ) : <h1>NO DATA FOUND</h1>
            }
            { node7.length !== 0 ?
              (
                <div className="border-2 border-black text-center py-5 px-5">
                  <center>
                  <img src="/sun.png" className="w-1/5" />
                  </center>
                  <h1 className="leading-7 font-bold text-xl">{node7.field_title_header_paragraph}
                  <p className="font-light text-xl">{node7.field_header_places_description}</p>
                  </h1>
                </div>
              ) : <h1>NO DATA FOUND</h1>
            }
            { node8.length !== 0 ?
              (
                <div className="border-2 border-black text-center py-5 px-5">
                  <center>
                  <img src="/airplane.png" className="w-1/5" />
                  </center>
                  <h1 className="leading-7 font-bold text-xl">{node8.field_title_header_paragraph}
                  <p className="font-light text-xl">{node8.field_header_places_description}</p>
                  </h1>
                </div>
              ) : <h1>NO DATA FOUND</h1>
            }
          </div>
          <div className="px-10 py-5">
            { node2.length !== 0 ?
              (
                  <div className="grid grid-cols-1 md:grid-cols-2 gap-x-10">
                      <p className="text-xl leading-10 text-justify text-black bg-gray-300 rounded-tl-full rounded-br-full" 
                      dangerouslySetInnerHTML={{__html:node2.field_body.processed}} />
                      <img className="w-full h-full" src={node2.relationships.field_upload_image.publicUrl} />
                  </div>
              ) : <h1>NO DATA FOUND</h1>
            }
            { node6.length !== 0 ?
              (
                  <div className="grid grid-cols-1 md:grid-cols-2 gap-x-10 mt-5 mb-5">
                    <img className="w-full h-full" src={node6.relationships.field_upload_image.publicUrl} />
                      <p className="text-xl leading-10 text-justify text-black bg-gray-300 rounded-tl-full rounded-br-full" 
                      dangerouslySetInnerHTML={{__html:node6.field_body.processed}} />
                  </div>
              ) : <h1>NO DATA FOUND</h1>
            }
          <hr className="bg-gray-800 h-0.5" />
          </div>
      <div className="px-4 py-5 sm:px-6 lg:px-8">
      <div className="flex flex-col justify-between sm:flex-row">
          <div className="grid grid-cols-1 md:grid-cols-2">
          {
              node4.length !== 0 ?
              node4.map((node, index) => (
              <div key={index} className="self-center">
                <h1 className="text-xl text-black">{node.field_title_places_location}
                <p className="font-light">{node.field_description_places}</p>
                </h1>
              </div>
              )) : <h1>NO DATA FOUND</h1>
            }
          </div>
            {
              node3.length !== 0 ?
              node3.map((node, index) => (
              <div key={index}>
                <h1 className="text-xl text-black">{node.field_location.title}</h1>
                <iframe src={node.field_location.uri} width="100%" height="300" />
              </div>
              )) : <h1>NO DATA FOUND</h1>
            }
          </div>
      </div>
          <ScrollToTop smooth />
      </Layout>
        )
      }

export const query = graphql`
query {
    allNodeFamousPlaces(filter: {id: {eq: "1030c46b-bbd5-5cf4-aca1-ca773eb3924e"}}) {
      nodes {
        relationships {
          field_background_ {
            publicUrl
          }
        }
        title
        body {
          processed
        }
      }
    }
    allParagraphPlaceParagraph(
      filter: {id: {in: ["09db4534-64d0-5fa2-980f-2d806764ce83", "9d15c572-e9a2-589e-9141-240ffc8f14a8"]}}
    ) {
      nodes {
        field_body {
          processed
        }
        relationships {
          field_upload_image {
            publicUrl
          }
        }
        id
      }
    }
    allBlockContentMap(filter: {id: {eq: "cc3d8bdc-e4f5-5754-98df-b3c188d34587"}}) {
      nodes {
        field_location {
          title
          uri
        }
      }
    }
    allParagraphContactPlacesParagraph(
      filter: {id: {in: ["6b78dbf7-a729-5765-8f04-410f89f8f1a5", "ce7026f5-18b9-56b3-85da-8a50626cee20", "25d49a22-6882-5867-bdac-2c5dda835b09", "7a1d2afa-2359-5d97-9d5c-6d3223b7fa50"]}}
    ) {
      nodes {
        field_title_places_location
        field_description_places
        id
      }
    }
    allParagraphHeaderPlaceParagraph(filter: {id: {in: ["e694c5d2-4115-5b43-90f6-ac6261a2936a", "a46f4e65-c54b-5807-825b-7960cb6ea8b2","e15cb707-5920-5ac1-bf7c-29ba11f8c5ee"]}}) {
      nodes {
        field_title_header_paragraph
        field_header_places_description
        id
      }
    }
  }
`

export default wardslake
