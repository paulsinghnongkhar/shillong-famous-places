import React from 'react';
import Layout from '../component/Layout';
import { graphql } from 'gatsby';
import ScrollToTop from 'react-scroll-to-top';

function taxiservices({ data }) {
  const node1 = data.allNodePlanner.nodes || [];
  const node2 = data.allParagraphTableParagraph.nodes || [];

  return (
    <Layout>
      <title>Shillong Famous Places</title>
      <div className="mt-5">
        {node1.length !== 0 ? (
          node1.map((noda, index) => (
            <div key={index}>
              <h1 className="text-center text-5xl text-black mt-5 border-2 py-5 border-black">
                {noda.title}
              </h1>
              <img className="w-full" src={noda.relationships.field_header_image_planner.publicUrl} />
              <div className="px-5 py-5 bg-gray-300 gap-x-2 gap-y-2">
                <div className="border-2 border-black">
                  {noda.relationships.field_planner_paragraph.map((node, index) => (
                    <div key={index} className="mt-5">
                      <h1 className="text-center text-3xl text-black">{node.field_title_planner}</h1>
                    </div>
                  ))}
                  <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-5 text-black px-5 py-5">
                    {node2.map((node, index) => (
                      <div key={index}>
                        <div className="border-2 border-black py-2">
                          <h1 className="text-center text-xl font-bold -mb-1">{node.field_table_title}</h1>
                        </div>
                        <div className="border-2 border-black py-2">
                          <p className="w-72 text-center -mb-1">{node.field_table_row[0]}</p>
                        </div>
                        <div className="border-2 border-black py-2">
                          <p className="w-72 text-center -mb-1">{node.field_table_row[1]}</p>
                        </div>
                        <div className="border-2 border-black py-2">
                          <p className="w-72 text-center -mb-1">{node.field_table_row[2]}</p>
                        </div>
                      </div>
                    ))}
                  </div>
                </div>
              </div>
            </div>
          ))
        ) : (
          <h1>NO DATA FOUND</h1>
        )}
      </div>
      <ScrollToTop smooth />
    </Layout>
  );
}

export const query = graphql`
  query {
    allNodePlanner(filter: { id: { eq: "2daa2b15-7150-5ce4-a472-3759218064c2" } }) {
      nodes {
        title
        relationships {
          field_header_image_planner {
            publicUrl
          }
          field_planner_paragraph {
            field_title_planner
          }
        }
      }
    }
    allParagraphTableParagraph {
      nodes {
        field_table_title
        field_table_row
      }
    }
  }
`;

export default taxiservices;
