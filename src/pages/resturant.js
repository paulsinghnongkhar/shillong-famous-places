import React from 'react'
import Layout from '../component/Layout'
import { graphql } from 'gatsby'
import ScrollToTop from "react-scroll-to-top";

function resturant({data}) {

    const node1 = data.allNodeDiscover.nodes || []

  return (
    <Layout>
      <title>Shillong Famous Places</title>
      <div className="mt-5">
        {node1.length !== 0 ? (
          node1.map((noda, index) => (
            <div key={index}>
              <h1 className="text-center text-5xl text-black mt-5 border-2 py-5 border-black">{noda.title}</h1>
              <img className="w-full" src={noda.relationships.field_discover_header_image.publicUrl} />
              <div className="grid grid-cols-1 md:grid-cols-3 px-5 py-5 bg-gray-300 gap-x-2 gap-y-2">
                {noda.relationships.field_body.map((node, index) => (
                  <div key={index} className="border-2 border-black px-5 py-5">
                    <h1 className="text-black text-xl font-bold">{node.field_title}</h1>
                    <p className="text-black text-lg leading-7" dangerouslySetInnerHTML={{ __html: node.field_description.processed }} />
                  </div>
                ))}
              </div>
            </div>
          ))
        ) : (
          <h1>NO DATA FOUND</h1>
        )}
      </div>
      <ScrollToTop smooth />
    </Layout>
  )
}

export const query = graphql
`
query {
    allNodeDiscover(filter: {id: {eq: "8ba97c61-9cc9-58a9-83d5-cf66021937d2"}}) 
    {
      nodes {
        title
        relationships {
            field_discover_header_image {
                publicUrl
            }
          field_body {
            field_title
            field_description {
              processed
            }
          }
        }
      }
    }
  }
`

export default resturant
