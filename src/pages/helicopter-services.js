import React from 'react';
import Layout from '../component/Layout';
import { graphql } from 'gatsby';
import ScrollToTop from "react-scroll-to-top";

function helicopterservices({ data }) {
  const node1 = data.allNodePlanner.nodes || [];

  return (
    <Layout>
      <title>Shillong Famous Places</title>
      <div className="mt-5">
        {node1.length !== 0 ? (
          node1.map((noda, index) => (
            <div key={index}>
              <h1 className="text-center text-5xl text-black mt-5 border-2 py-5 border-black">{noda.title}</h1>
              <img className="w-full" src={noda.relationships.field_header_image_planner.publicUrl} />
              <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-2 xl:grid-cols-2 px-5 py-5 bg-gray-300 gap-x-2 gap-y-2">
                {noda.relationships.field_planner_paragraph.map((node, index) => (
                  <div key={index} className="border-2 border-black px-5 py-5">
                    <h1 className="text-black text-xl font-bold">{node.field_title_planner}</h1>
                    <p className="text-black text-lg leading-7" dangerouslySetInnerHTML={{ __html: node.field_description_planner.processed }} />
                  </div>
                ))}
              </div>
            </div>
          ))
        ) : (
          <h1>NO DATA FOUND</h1>
        )}
      </div>
      <ScrollToTop smooth />
    </Layout>
  );
}

export const query = graphql`
  query {
    allNodePlanner(filter: { id: { eq: "fe332323-e720-5f02-9429-fd31b4d6d7ee" } }) {
      nodes {
        path {
          alias
        }
        title
        relationships {
          field_header_image_planner {
            publicUrl
          }
          field_planner_paragraph {
            field_title_planner
            field_description_planner {
              processed
            }
          }
        }
      }
    }
  }
`;

export default helicopterservices;
