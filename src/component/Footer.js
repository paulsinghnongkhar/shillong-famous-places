import { graphql, useStaticQuery } from 'gatsby'
import React from 'react'

function Footer() {
  const query = useStaticQuery(graphql`
    query {
      allBlockContentBasic(
        filter: { id: { in: ["e1ee8e7c-7342-5c2a-8c50-1546bc18f920"] } }
      ) {
        nodes {
          info
          body {
            processed
          }
          id
        }
      }
      allBlockContentCopyright {
        nodes {
          body {
            processed
          }
        }
      }
      allBlockContentMap(filter: { id: { eq: "9f18d541-74ed-52e4-be51-40a4506b6ab5" } }) {
        nodes {
          field_location {
            title
            uri
          }
        }
      }
    }
  `)

  const nodes = query.allBlockContentBasic.nodes;
  const node2 = query.allBlockContentCopyright.nodes;
  const node3 = query.allBlockContentMap.nodes;

  return (
    <div className="px-4 py-5 text-white bg-gray-800 sm:px-6 lg:px-8">
      <div className="flex flex-col justify-between sm:flex-row">
        {nodes.map((node, index) => (
          <div key={index} className="self-center">
            <h1 className="text-xl">{node.info}</h1>
            <p className="text-white no-underline" dangerouslySetInnerHTML={{ __html: node.body.processed }} />
          </div>
        ))}
        {node3.map((node, index) => (
          <div key={index}>
            <h1 className="text-xl">{node.field_location.title}</h1>
            <iframe src={node.field_location.uri} width="100%" height="300" />
          </div>
        ))}
      </div>
      {node2.map((node, index) => (
        <center key={index}>
          <p className="text-center" dangerouslySetInnerHTML={{ __html: node.body.processed }} />
        </center>
      ))}
    </div>
  )
}

export default Footer

