import React from "react";
import { useStaticQuery, graphql } from "gatsby";

const Article = () => {
  const data = useStaticQuery(graphql`
    query {
      nodeArticle {
        title
        relationships {
          field_image {
            publicUrl
          }
        }
      }
    }
  `);

  const article = data.nodeArticle;

  return (
    <div className="flex flex-col md:flex-row bg-gray-800">
      <img
        className="rounded-full mt-5 ml-5 w-20 h-20 md:w-40 md:h-40"
        src={article.relationships.field_image.publicUrl}
        alt={article.title}
      />
      <h1 className="text-white text-5xl mt-5 ml-5 md:mt-14 md:ml-10">
        {article.title}
      </h1>
    </div>
  );
};

export default Article;
