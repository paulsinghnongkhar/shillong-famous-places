import React, { useState } from 'react';
import { useStaticQuery, graphql, Link } from 'gatsby';

const MenuLinks = () => {
  const [menuOpen, setMenuOpen] = useState(false); // State to manage mobile menu visibility
  const data = useStaticQuery(graphql`
    query {
      allNodeHome {
        nodes {
          title
          path {
            alias
          }
        }
      }
      allNodeMenu(sort: { title: DESC }) {
        nodes {
          title
          path {
            alias
          }
        }
      }
    }
  `);

  const menuLinks = data.allNodeMenu.nodes;
  const menuLink2 = data.allNodeHome.nodes;

  return (
    <div className="py-3 bg-gray-300">
      <div className="container px-4 mx-auto">
        {/* Mobile Menu Toggle Button */}
        <div className="sm:hidden">
          <button
            onClick={() => setMenuOpen(!menuOpen)}
            className="block p-2 text-3xl text-black border border-black focus:outline-none"
          >
            {menuOpen ? 'Close' : 'Menu'}
          </button>
        </div>

        {/* Mobile Menu */}
        {menuOpen && (
          <div className="sm:hidden">
            {menuLink2.map((link, index) => (
              <div key={index} className="mb-4">
                <Link
                  onClick={() => setMenuOpen(false)} // Close the menu when a link is clicked
                  className="block px-2 py-2 text-3xl text-black no-underline border-black hover:border-y-2"
                  to="/"
                >
                  {link.title}
                </Link>
              </div>
            ))}
            {menuLinks.map((link, index) => (
              <div key={index} className="mb-4">
                <Link
                  onClick={() => setMenuOpen(false)} // Close the menu when a link is clicked
                  className="block px-2 py-2 text-3xl text-black no-underline border-black hover:border-y-2"
                  to={link.path.alias}
                >
                  {link.title}
                </Link>
              </div>
            ))}
          </div>
        )}

        {/* Desktop Menu */}
        <div className="flex-wrap justify-center hidden sm:flex">
          {menuLink2.map((link, index) => (
            <div key={index} className="mb-4 sm:mb-0 sm:mr-4">
              <Link
                className="px-2 text-3xl text-black no-underline border-black hover:border-y-2"
                to="/"
              >
                {link.title}
              </Link>
            </div>
          ))}
          {menuLinks.map((link, index) => (
            <div key={index} className="mb-4 sm:mb-0 sm:mr-4">
              <Link
                className="px-2 text-3xl text-black no-underline border-black hover:border-y-2"
                to={link.path.alias}
              >
                {link.title}
              </Link>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default MenuLinks;
